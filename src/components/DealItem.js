import React, {Component} from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {priceDisplay} from '../util';

class DealItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  static propTypes = {
    deal: PropTypes.object.isRquired,
    onPress: PropTypes.func.isRequired,
  };
  handlePress = () => {
    console.log(this.props.deal.key);
    this.props.onPress(this.props.deal.key);
  };

  render() {
    const {deal} = this.props;
    return (
      <TouchableOpacity style={styles.deal} onPress={this.handlePress}>
        <View style={styles.deal}>
          <Image
            source={{uri: this.props.deal.media[0]}}
            style={styles.image}
          />
          <View style={styles.info}>
            <Text style={styles.title}>{deal.title}</Text>
            <View style={styles.footer}>
              <Text style={styles.cause}>{deal.cause.name}</Text>
              <Text style={styles.price}>{priceDisplay(deal.price)}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 150,
    backgroundColor: '#CCC',
  },
  deal: {
    marginHorizontal: 12,
    marginTop: 12,
  },
  info: {
    padding: 10,
    backgroundColor: '#fff',
    borderColor: '#bbb',
    borderWidth: 1,
    borderTopWidth: 0,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  footer: {
    flexDirection: 'row',
  },
  cause: {
    flex: 2,
  },
  price: {
    flex: 1,
    textAlign: 'right',
  },
});

export default DealItem;
