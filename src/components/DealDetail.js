import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  PanResponder,
  Animated,
  Dimensions,
  Linking,
  Button,
  ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import {priceDisplay} from '../util';
import ajax from '../ajax';

class DealDetail extends Component {
  imageXPos = new Animated.Value(0);
  imagePanResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onPanResponderMove: (evt, gs) => {
      this.imageXPos.setValue(gs.dx);
    },
    onPanResponderRelease: (evt, gs) => {
      this.width = Dimensions.get('window').width;
      if (Math.abs(gs.dx) > this.width * 0.4) {
        const direction = Math.sign(gs.dx);
        Animated.timing(this.imageXPos, {
          toValue: direction * this.width,
          duration: 250,
        }).start(() => this.handleSwipe(-1 * direction));
      } else {
        Animated.spring(this.imageXPos, {
          toValue: 0,
        }).start();
      }
    },
  });

  handleSwipe = indexDirection => {
    if (!this.state.deal.media[this.state.imageIndex + indexDirection]) {
      Animated.spring(this.imageXPos, {
        toValue: 0,
      }).start();
      return;
    }
    this.setState(
      prevState => ({
        imageIndex: prevState.imageIndex + indexDirection,
      }),
      () => {
        this.imageXPos.setValue(indexDirection * this.width);
        Animated.spring(this.imageXPos, {
          toValue: 0,
        }).start();
      },
    );
  };
  openDealURL = () => {
    Linking.openURL(this.state.deal.url);
  };
  constructor(props) {
    super(props);
    this.state = {
      deal: this.props.initialDealData,
    };
  }
  static propTypes = {
    initialDealData: PropTypes.object.isRquired,
    onBack: PropTypes.func.isRequired,
  };

  async componentDidMount() {
    const fulldeal = await ajax.fetchInitialDealDetails(this.state.deal.key);
    console.log(fulldeal);
    this.setState({
      deal: fulldeal,
      imageIndex: 0,
    });
  }

  handlePress = () => {};

  render() {
    console.log(deal);
    const {deal} = this.state;
    return (
      <View style={styles.deal}>
        <TouchableOpacity onPress={this.props.onBack}>
          <Text style={styles.backButton}> Back</Text>
        </TouchableOpacity>
        <Animated.Image
          {...this.imagePanResponder.panHandlers}
          source={{uri: deal.media[this.state.imageIndex]}}
          style={[styles.image, {left: this.imageXPos}]}
        />
        <View style={styles.detail}>
          <Text style={styles.title}>{deal.title}</Text>
        </View>
        <ScrollView>
          <View style={styles.footerHead}>
            <View style={styles.footer}>
              <Text style={styles.price}>{priceDisplay(deal.price)}</Text>
              <Text style={styles.cause}>{deal.cause.name}</Text>
            </View>
            {deal.user && (
              <View>
                <Image source={{uri: deal.user.avatar}} style={styles.avatar} />
                <Text>{deal.user.name}</Text>
              </View>
            )}
          </View>
          <View style={styles.description}>
            <Text>{deal.description}</Text>
          </View>
        </ScrollView>

        <Button title="Buy this deal!" onPress={this.openDealURL} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  deal: {
    marginTop: 40,
  },
  image: {
    width: '100%',
    height: 150,
    backgroundColor: '#CCC',
  },
  backButton: {
    marginBottom: 5,
    color: '#22f',
    marginLeft: 15,
  },
  footerHead: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 10,
    marginBottom: 5,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    padding: 10,
    backgroundColor: 'rgba(237,149,45,0.4)',
  },
  footer: {
    // flexDirection: 'row',
    // justifyContent: 'space-around',
    // alignItems: 'center',
    // marginTop: 15,
  },

  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  info: {
    alignItems: 'center',
  },
  user: {
    alignItems: 'center',
  },
  description: {
    padding: 10,
    borderColor: '#FF0',
  },
});

export default DealDetail;
