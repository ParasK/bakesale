import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Easing,
  Animated,
  Dimensions,
} from 'react-native';
import Ajax from '../ajax';
import DealList from './DealList';
import DealDetail from './DealDetail';
import SearchBar from './SearchBar';
import ajax from '../ajax';

class App extends Component {
  titleXPos = new Animated.Value(0);
  constructor(props) {
    super(props);
    this.state = {
      deals: [],
      dealsFromSearch: [],
      currentDealId: null,
      activeSearchTerm: '',
    };
  }
  animateTitle = (direction = 1) => {
    const width = Dimensions.get('window').width - 150;
    Animated.timing(this.titleXPos, {
      toValue: -direction * (width / 2),
      duration: 1000,
      easing: Easing.ease,
    }).start(({finished}) => {
      if (finished) {
        this.animateTitle(-1 * direction);
      }
    });
  };
  async componentDidMount() {
    this.animateTitle();
    const deals = await Ajax.fetchInitialDeals();
    this.setState({deals});
  }
  setCurrentDeal = dealId => {
    this.setState({
      currentDealId: dealId,
    });
  };

  unSetCurrentDeal = dealId => {
    this.setState({
      currentDealId: null,
    });
  };

  searchDeals = async searchTerm => {
    let dealsFromSearch = [];
    if (searchTerm) {
      dealsFromSearch = await ajax.fetchDealSearchResult(searchTerm);
    }
    this.setState({dealsFromSearch, activeSearchTerm: searchTerm});
  };

  clearSearch = () => {
    this.setState({dealsFromSearch: []});
  };

  currentDeal = () => {
    return this.state.deals.find(deal => deal.key === this.state.currentDealId);
  };

  render() {
    if (this.state.currentDealId) {
      return (
        <DealDetail
          initialDealData={this.currentDeal()}
          onBack={this.unSetCurrentDeal}
        />
      );
    }
    const dealsToDisplay =
      this.state.dealsFromSearch.length > 0
        ? this.state.dealsFromSearch
        : this.state.deals;
    if (dealsToDisplay.length > 0) {
      return (
        <View style={styles.main}>
          <SearchBar
            searchDeals={this.searchDeals}
            initialSearchTerm={this.state.activeSearchTerm}
          />
          <DealList deals={dealsToDisplay} onItemPress={this.setCurrentDeal} />
        </View>
      );
    }
    return (
      <Animated.View style={[styles.container, {left: this.titleXPos}]}>
        <Text style={styles.header}> BakeSale </Text>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    fontSize: 40,
  },
  main: {
    marginTop: 40,
  },
});
export default App;
