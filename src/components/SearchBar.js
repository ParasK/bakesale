import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import debounce from 'lodash.debounce';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: this.props.initialSearchTerm,
    };
  }

  static propTypes = {
    searchDeals: PropTypes.func.isRequired,
    initialSearchTerm: PropTypes.string.isRequired,
  };

  searchDeals = searchTerm => {
    this.props.searchDeals(searchTerm);
    this.inputElement.blur();
  };

  debouncedSearchDeals = debounce(this.searchDeals, 300);
  handleChange = searchTerm => {
    this.setState({searchTerm}, () => {
      this.debouncedSearchDeals(this.state.searchTerm);
    });
  };

  render() {
    return (
      <TextInput
        ref={inputElement => {
          this.inputElement = inputElement;
        }}
        value={this.state.searchTerm}
        style={styles.input}
        placeholder="Search All delas"
        onChangeText={this.handleChange}
      />
    );
  }
}

const styles = StyleSheet.create({
  input: {
    height: 40,
    marginHorizontal: 12,
  },
});
export default SearchBar;
